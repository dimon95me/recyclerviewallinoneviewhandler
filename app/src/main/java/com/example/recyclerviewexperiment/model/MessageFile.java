package com.example.recyclerviewexperiment.model;

import android.net.Uri;

import com.example.recyclerviewexperiment.enumerations.FileType;

import java.io.File;

import lombok.Data;

@Data
class MessageFile {

    private String fileId;
    private String fileName;
    private String mimeType;
    private File file;
    private Uri uriFile;
    private FileType fileType;
    private double fileSize;
    private String fileCategory;
    private String fileUrl;
    private String fileDescription;
    private String fileWidth;
    private String fileHeight;
    private String fileCompress;
    private String imageUrl;
    private String thumbUrl;
    private String thumbSmallUrl;
}

