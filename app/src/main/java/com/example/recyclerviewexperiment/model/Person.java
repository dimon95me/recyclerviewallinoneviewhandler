package com.example.recyclerviewexperiment.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Person {
    private String nickname;
    private String firstName;
    private String lastName;
    private boolean isOnline;
    private Integer lastTimeOnline;
    private String groupRole;
    private String avatar;
    private String userId;
    private String country;
    private String lang;
    private String contactName;
    private String contactPhone;
}
