package com.example.recyclerviewexperiment.model;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.util.Date;
import java.util.List;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class Message implements/* IMessage,MessageContentType.Image,*/ MessageContentType {
    private String id;
    private String text;
    private Date createdAt;
    private User user;
    private String image;
    private Voice voice;
    private String sharedContactId;
    private boolean isDisplayed;
    private boolean isDelivered;
    private boolean isUpdated;
    private boolean withName;
    private Person sharedContact;
    private List<MessageFile> messageFiles;
    private Sticker sticker;
    private LatLng geolocation;
    private String translatedMessage;
    private Message repliedMessage;
    private Boolean isForwarded;
    private String forwarderName;
    private String forwarderId;
//    private Boolean isOwnMessage;

    @Override
    public String getId() {
        return id;
    }

//    @Override
    public String getText() {
        return text;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    /*@Nullable
    @Override
    public String getImageUrl() {
        return image;
    }*/


    @Data
    public static class Voice {

        private String url;
        private int duration;

        public Voice(String url, int duration) {
            this.url = url;
            this.duration = duration;
        }

        public String getUrl() {
            return url;
        }

        public int getDuration() {
            return duration;
        }
    }
}
