package com.example.recyclerviewexperiment.model;

import android.net.Uri;

class Sticker {

    String packId;
    String stickerId;
    Uri stickerUrl;
}
