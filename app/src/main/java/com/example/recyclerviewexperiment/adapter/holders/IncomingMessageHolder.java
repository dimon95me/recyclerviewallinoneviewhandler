package com.example.recyclerviewexperiment.adapter.holders;

import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.example.recyclerviewexperiment.databinding.ItemMessageBinding;
import com.example.recyclerviewexperiment.model.Message;
import com.stfalcon.chatkit.messages.MessageHolders;

public class IncomingMessageHolder extends MessageHolders.IncomingTextMessageViewHolder<Message> {

    ItemMessageBinding itemMessageBinding;
    HolderWorker holderWorker;

    public IncomingMessageHolder(View itemView, Object payload) {
        super(itemView, payload);
        itemMessageBinding = DataBindingUtil.bind(itemView);
        holderWorker = new HolderWorker(itemMessageBinding);
    }

    @Override
    public void onBind(Message message) {
//        super.onBind(message);
        holderWorker.setMessage(message);
    }
}
