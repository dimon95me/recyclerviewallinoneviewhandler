package com.example.recyclerviewexperiment.adapter.holders;

import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewStubProxy;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.recyclerviewexperiment.MainActivity;
import com.example.recyclerviewexperiment.R;
import com.example.recyclerviewexperiment.databinding.ItemMessageBinding;
import com.example.recyclerviewexperiment.databinding.ItemMessagePartAvatarBinding;
import com.example.recyclerviewexperiment.databinding.ItemMessagePartContactBinding;
import com.example.recyclerviewexperiment.databinding.ItemMessagePartForwardBinding;
import com.example.recyclerviewexperiment.databinding.ItemMessagePartImageBinding;
import com.example.recyclerviewexperiment.databinding.ItemMessagePartReplyBinding;
import com.example.recyclerviewexperiment.databinding.ItemMessagePartTextBinding;
import com.example.recyclerviewexperiment.model.Message;

import java.util.Calendar;
import java.util.Date;

public class HolderWorker {

    public static String ownUserId = MainActivity.MY_ID;
    public static String forwardTemplateText = "Forwarded by ";
    public static String editedTemplateText = "Edited";
    public static boolean isGroupChat = true;
    public static String attachment = "Attachment: ";

    private Message message;
    private ItemMessageBinding binding;

    public HolderWorker(ItemMessageBinding binding) {
        this.binding = binding;
        initializeStubListeners();
    }

    public void setMessage(Message message) {
        this.message = message;
        binding.setMessage(message);
        updateView();
    }

    public void updateView() {
        formatViewCondition();
        if (message.getIsForwarded()!=null&&message.getIsForwarded()) {
            verifyIfMessageDataExists(message.getIsForwarded(), binding.stubMessageForward);
        } else {
//            verifyIfMessageDataExists(message.getRepliedMessage(), binding.stubMessageReply);
        }
        verifyIfMessageDataExists(message.getText(), binding.stubMessageText);
        verifyIfMessageDataExists(message.getImage(), binding.stubMessageImage);
        verifyIfMessageDataExists(message.getSharedContact(), binding.stubMessageContact);
    }

    private void formatViewCondition() {
        if (isOwnMessage()) {
            binding.mainLayout.setGravity(Gravity.RIGHT);
            binding.llForBubbleGravity.setGravity(Gravity.RIGHT);
            LayoutParams tvTextAndTimeParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            tvTextAndTimeParams.gravity = Gravity.RIGHT;
            binding.llTextAndTime.setLayoutParams(tvTextAndTimeParams);
//            binding.messageUserAvatar.setVisibility(View.GONE);
        } else {
            if (isGroupChat) {
                verifyIfMessageDataExists(message.getUser().getAvatar(), binding.stubMessageAvatar);
            }
        }

        checkIsReaded();
        setIfEdited();
        applyDateToView(binding.messageTime);
    }

    private void initializeStubListeners() {
        binding.stubMessageAvatar.setOnInflateListener(new ViewStub.OnInflateListener() {
            @Override
            public void onInflate(ViewStub stub, View inflated) {
                ItemMessagePartAvatarBinding avatarBinding = DataBindingUtil.bind(inflated);
                setImage(inflated, avatarBinding.ivMessageAvatar, message.getUser().getAvatar(), true);
            }
        });

        binding.stubMessageReply.setOnInflateListener(new ViewStub.OnInflateListener() {
            @Override
            public void onInflate(ViewStub viewStub, View view) {
                ItemMessagePartReplyBinding replyBinding = DataBindingUtil.bind(view);
//                replyBinding.setMessage(message.getRepliedMessage());
            }
        });
        binding.stubMessageForward.setOnInflateListener(new ViewStub.OnInflateListener() {
            @Override
            public void onInflate(ViewStub viewStub, View view) {
                ItemMessagePartForwardBinding forwardBinding = DataBindingUtil.bind(view);
                binding.setMessage(message);
                String forwardText = forwardTemplateText;
                if (message.getForwarderName() != null && !message.getForwarderName().isEmpty()) {
                    forwardText += message.getForwarderName();
                } else forwardText += "Unknown";
                forwardBinding.tvMessageForward.setText(forwardText);
            }
        });


        binding.stubMessageText.setOnInflateListener(new ViewStub.OnInflateListener() {
            @Override
            public void onInflate(ViewStub viewStub, View view) {
                ItemMessagePartTextBinding textBinding = DataBindingUtil.bind(view);
                textBinding.setMessage(message);
//                if (message.getText().length() > 500 && textBinding.tvMessageTranslatedText == null) {//Not working because not initialized
//                    binding.llTextAndTime.setOrientation(LinearLayout.HORIZONTAL);
//                    LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    textViewParams.gravity = Gravity.CENTER_VERTICAL;
//                    binding.messageTime.setLayoutParams(textViewParams);
//                }
                if (isOwnMessage()) {
                    textBinding.tvMessageText.setTextColor(Color.WHITE);
                } else {
                    textBinding.tvMessageText.setTextColor(Color.BLACK);
                }
                if (message.getTranslatedMessage() != null && !message.getTranslatedMessage().isEmpty()) {
                    textBinding.tvMessageTranslatedText.setVisibility(View.VISIBLE);
                    textBinding.vMessageTranslatedTextDivider.setVisibility(View.VISIBLE);
                    textBinding.tvMessageText.setTextColor(Color.GRAY);
                    if (isOwnMessage()) {
                        textBinding.tvMessageTranslatedText.setTextColor(Color.WHITE);
                    } else {
                        textBinding.tvMessageTranslatedText.setTextColor(Color.BLACK);
                    }
                }
            }
        });

        binding.stubMessageImage.setOnInflateListener(new ViewStub.OnInflateListener() {
            @Override
            public void onInflate(ViewStub stub, View inflated) {
                ItemMessagePartImageBinding imageBinding = DataBindingUtil.bind(inflated);
                setImage(inflated, imageBinding.ivMessageImage, message.getImage(), false);
            }
        });

        binding.stubMessageContact.setOnInflateListener(new ViewStub.OnInflateListener() {
            @Override
            public void onInflate(ViewStub viewStub, View view) {
                ItemMessagePartContactBinding contactBinding = DataBindingUtil.bind(view);
                contactBinding.setMessage(message);
                binding.llTimeAndCheck.setVisibility(View.GONE);
                setImage(view, contactBinding.messageContactAvatar, message.getSharedContact().getAvatar(), false);
                applyDateToView(contactBinding.tvMessageContactTime);
                checkIsReaded(contactBinding.messageContactCheck);
                if (!isOwnMessage()) {
                    contactBinding.tvMessageContactActionViewContact.setBackgroundResource(R.drawable.bg_incoming_messge_action_view_contact);
                }
            }
        });
    }

    private void setIfEdited() {
        if (message.isUpdated()) {
            binding.messageEdited.setVisibility(View.VISIBLE);
            binding.messageEdited.setText(editedTemplateText);
        }
    }

    private boolean isOwnMessage() {
        if (message.getUser().getId() == null) {
            throw new NullPointerException("message user id in HolderWorker is null");
        }
        if (ownUserId == null) {
            throw new NullPointerException("static own user id in HolderWorker is null");
        }
        return message.getUser().getId().equals(ownUserId);
    }

    private void setImage(View view, ImageView image, String src, Boolean isCircle) {
        if (isCircle) {
            Glide.with(view.getContext())
                    .load(src)
                    .apply(RequestOptions.circleCropTransform())
                    .error(R.drawable.ic_oops)
                    .into(image);
        } else {
            Glide.with(view.getContext())
                    .load(src)
                    .error(R.drawable.ic_oops)
                    .into(image);
        }
    }

    private void applyDateToView(TextView textView) {
        String time = "";
        Date date = message.getCreatedAt();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int i = calendar.get(Calendar.MINUTE);
        String minute = calendar.get(Calendar.MINUTE) < 10 ? "0" + calendar.get(Calendar.MINUTE) : String.valueOf(calendar.get(Calendar.MINUTE));
        textView.setText(calendar.get(Calendar.HOUR_OF_DAY) + ":" + minute);
    }

    private void verifyIfMessageDataExists(Object parameter, ViewStubProxy viewStubProxy) {
        if (parameter != null) {
            if (!viewStubProxy.isInflated()) {
                viewStubProxy.getViewStub().inflate();
            }
        }
    }

    private void checkIsReaded() {
        if (!message.isDelivered()) {
            binding.ivMessageCheck.setVisibility(View.GONE);
            return;
        }
        if (message.isDelivered() && !message.isDisplayed()) {
            binding.ivMessageCheck.setImageDrawable(binding.getRoot().getResources().getDrawable(R.drawable.ic_tick_unread));
            return;
        }
        if (message.isDisplayed()) {
            binding.ivMessageCheck.setImageDrawable(binding.getRoot().getResources().getDrawable(R.drawable.ic_tick_read));
            return;
        }
    }

    private void checkIsReaded(ImageView view) {
        if (!message.isDelivered()) {
            view.setVisibility(View.GONE);
            return;
        }
        if (message.isDelivered() && !message.isDisplayed()) {
            view.setImageDrawable(binding.getRoot().getResources().getDrawable(R.drawable.ic_tick_unread));
            return;
        }
        if (message.isDisplayed()) {
            view.setImageDrawable(binding.getRoot().getResources().getDrawable(R.drawable.ic_tick_read));
            return;
        }
    }
}
