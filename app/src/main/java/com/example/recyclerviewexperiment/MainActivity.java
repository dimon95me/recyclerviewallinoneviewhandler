package com.example.recyclerviewexperiment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.recyclerviewexperiment.adapter.MLAdapter;
import com.example.recyclerviewexperiment.adapter.holders.IncomingMessageHolder;
import com.example.recyclerviewexperiment.adapter.holders.OutcomingMessageHolder;
import com.example.recyclerviewexperiment.databinding.ActivityMainBinding;
import com.example.recyclerviewexperiment.model.Message;
import com.example.recyclerviewexperiment.model.Person;
import com.example.recyclerviewexperiment.model.User;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MessageHolders.ContentChecker<Message>, MessagesListAdapter.SelectionListener {

    public static final String MY_ID = "e4bb123e-fb14-474a-8490-46d620f9aaf6";
    private static final String PARTICIPANT_ID = "e4bb123e-fb14-474a-8490-46d620f9aaf7";
    private static final User MY_USER = new User("e4bb123e-fb14-474a-8490-46d620f9aaf6", "I Am", "https://img.favpng.com/1/18/17/ninja-icon-design-icon-png-favpng-KRYptt5LJ1a7u6ZttuMRvWFYc_t.jpg", true);
    private static final User PARTICIPANT_USER = new User("e4bb123e-fb14-474a-8490-46d620f9aaf7", "Not I Am", "https://img.favpng.com/12/23/8/angry-birds-go-icon-png-favpng-iGBKaDTbnSuTuvn0d559Tdgxp_t.jpg", true);
    private static final Person PERSON = new Person("Marty99", "Marty", "Zebra", true, 15, null,"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQdJrIlR_wM2-lXNN9jX5O54S2dDeL2oxIARxXbGxL4LN82mb1r", "e4bb123e-fb14-474a-8490-46d620f9aaf8", "Savanna", "Zebrian", "Matrhy Zebra", "+178783287378");
    private static final Person PERSON1 = new Person("Penguin Skipper", "Skipper", "Penguin", false, 150, null,"https://cdn.imgbin.com/13/24/11/imgbin-skipper-kowalski-penguin-madagascar-television-penguin-AWEDhyNZmaguzeWbgqAG7aCT3.jpg", "e4bb123e-fb14-474a-8490-46d620f9aaf9", "North pole", "Antarctica", "Skipper Penguin", "skipper.penguin@gmail.com");

    public static final byte ANY_TYPE = 1;


    List<Message> messageList = new ArrayList<>();
//    MessagesAdapter adapter;
//    RecyclerView recyclerView;

    ActivityMainBinding mainBinding;

    private MLAdapter mlAdapter;
    MessagesList mlMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generateMessages();
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        ImageLoader imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, @Nullable String url, @Nullable Object payload) {
                Glide.with(getApplicationContext()).load(url).into(imageView);
            }
        };
        mlMessages = mainBinding.mlMessages;

        MessageHolders messageHolders = new MessageHolders();
        messageHolders.registerContentType(ANY_TYPE, IncomingMessageHolder.class, R.layout.item_message, OutcomingMessageHolder.class, R.layout.item_message, this);


        mlAdapter = new MLAdapter(MY_USER.getId(), messageHolders, imageLoader);
        mlAdapter.enableSelectionMode(this);
        mlMessages.setAdapter(mlAdapter);
        for (Message message : messageList) {
            mlAdapter.addToStart(message, true);
        }
    }

    private void generateMessages() {
        Message ownTextForwardedMessage = Message.builder()
                .createdAt(new Date(System.currentTimeMillis()))
                .user(MY_USER)
                .id("4041381"+System.currentTimeMillis())
                .text("Own forwarded text what i writed")
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(ownTextForwardedMessage);

        Message ownLongTextForwardedMessage = Message.builder()
                .createdAt(new Date(System.currentTimeMillis()))
                .user(MY_USER)
                .id("4041381"+System.currentTimeMillis())
                .text("Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed ")
                .translatedMessage("Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed Own forwarded text what i writed ")
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(ownLongTextForwardedMessage);

        Message ownTextMessage = Message.builder()
                .forwarderId(MY_ID)
                .createdAt(new Date(System.currentTimeMillis()))
                .forwarderName("Alex the lion")
                .isForwarded(true)
                .id("4041381"+System.currentTimeMillis())
                .user(MY_USER)
                .text("Own text what i writed")
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(ownTextMessage);

        Message incomingTextMessage = Message.builder()
                .forwarderId(PARTICIPANT_ID)
                .forwarderName("Shrek")
                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .user(PARTICIPANT_USER)
                .text("Participant text what was writed")
                .isDelivered(true)
                .isDisplayed(true)
                .isUpdated(true)
                .build();
        addToMessagesList(incomingTextMessage);

        Message incomingImageMessage = Message.builder()
                .forwarderId(PARTICIPANT_ID)
                .forwarderName("Sherlock")
                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .user(PARTICIPANT_USER)
                .image("https://secure.gravatar.com/avatar/3791a8a63ad5caf4bbe1cb50f0aafeee?s=120&d=blank&r=g")
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(incomingImageMessage);

        Message incomingEmptyMessage = Message.builder()
                .forwarderId(PARTICIPANT_ID)
                .forwarderName("pokemon pikachu")
                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .user(PARTICIPANT_USER)
                .isDelivered(true)
                .isDisplayed(true)
                .isUpdated(true)
                .build();
        addToMessagesList(incomingEmptyMessage);

        Message outcomingContactMessage = Message.builder()
                .forwarderId(MY_ID)
                .forwarderName("Eaggle")
                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .sharedContact(PERSON)
                .user(MY_USER)
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(outcomingContactMessage);

        Message incomingContactMessage = Message.builder()
                .forwarderId(MY_ID)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .sharedContact(PERSON1)
                .user(PARTICIPANT_USER)
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(incomingContactMessage);

        Message incomingTranslatedMessage = Message.builder()
                .forwarderId(MY_ID)
                .forwarderName("Dr. Dullitl")
                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .text("Текстовое Cooбщение1")
                .translatedMessage("Text Message")
                .user(PARTICIPANT_USER)
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(incomingTranslatedMessage);

        Message outcomingTranslatedMessage = Message.builder()
                .forwarderId(MY_ID)
                .forwarderName("Eaggle")
                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .text("Текстовое Cooбщение2")
                .translatedMessage("Text Message")
                .sharedContact(PERSON)
                .user(MY_USER)
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(outcomingTranslatedMessage);

        Message outcomingRepliedTextMessage = Message.builder()
                .forwarderId(MY_ID)
                .forwarderName("Eaggle")
//                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .text("Текстовое Cooбщение3")
//                .translatedMessage("Text Message")
                .user(MY_USER)
                .repliedMessage(incomingTextMessage)
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(outcomingRepliedTextMessage);

        Message outcomingRepliedImageMessage = Message.builder()
                .forwarderId(MY_ID)
                .forwarderName("Eaggle")
//                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .text("Текстовое Cooбщение4")
//                .translatedMessage("Text Message")
                .user(MY_USER)
                .isDelivered(true)
                .repliedMessage(incomingImageMessage)
                .isDisplayed(true)
                .build();
        addToMessagesList(outcomingRepliedImageMessage);

        Message outcomingRepliedContactMessage = Message.builder()
                .forwarderId(MY_ID)
                .forwarderName("Eaggle")
//                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .text("Текстовое Cooбщение5")
//                .translatedMessage("Text Message")
                .user(MY_USER)
                .isDelivered(true)
                .repliedMessage(incomingContactMessage)
                .isDisplayed(true)
                .build();
        addToMessagesList(outcomingRepliedContactMessage);
//
        Message incomingRepliedTextMessage = Message.builder()
                .forwarderId(PARTICIPANT_ID)
                .forwarderName("Eaggle")
//                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .text("Текстовое Cooбщение6")
//                .translatedMessage("Text Message")
                .user(PARTICIPANT_USER)
                .repliedMessage(ownLongTextForwardedMessage)
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(incomingRepliedTextMessage);

        Message incomingRepliedImageMessage = Message.builder()
                .forwarderId(PARTICIPANT_ID)
                .forwarderName("Eaggle")
//                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .text("Текстовое Cooбщение7")
//                .translatedMessage("Text Message")
                .user(PARTICIPANT_USER)
                .isDelivered(true)
                .repliedMessage(incomingImageMessage)
                .isDisplayed(true)
                .build();
        addToMessagesList(incomingRepliedImageMessage);

        Message incomingRepliedContactMessage = Message.builder()
                .forwarderId(PARTICIPANT_ID)
                .forwarderName("Eaggle")
//                .isForwarded(true)
                .createdAt(new Date(System.currentTimeMillis()))
                .id("4041381"+System.currentTimeMillis())
                .text("Текстовое Cooбщение8")
//                .translatedMessage("Text Message")
                .user(PARTICIPANT_USER)
                .repliedMessage(incomingContactMessage)
                .isDelivered(true)
                .isDisplayed(true)
                .build();
        addToMessagesList(incomingRepliedContactMessage);
    }

    private void addToMessagesList(Message message) {
        messageList.add(message);
    }

    @Override
    public boolean hasContentFor(Message message, byte type) {
        switch (type) {
            case ANY_TYPE:
                return true;
        }
        return false;
    }

    @Override
    public void onSelectionChanged(int count) {

    }
}
