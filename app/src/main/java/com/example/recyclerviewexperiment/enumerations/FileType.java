package com.example.recyclerviewexperiment.enumerations;

public enum FileType {
    USER_AVATAR,
    GROUP_AVATAR,
    CHANNEL_AVATAR,
    FILE,
    FILE_IMAGE,
    FILE_VIDEO,
    FILE_AUDIO,
    STICKER
}